/*
 * Copyright 2010 Benny Halevy <bhalevy@panasas.com>
 *
 * Licensed under the GPL version 2 license.
 * See "COPYING" for more information
 */

#include "alloc.h"
#include "client.h"
#include "debug.h"
#include "layout.h"
#include "list.h"
#include "queue.h"
#include "rand.h"
#include "server.h"

struct queue cl_msg_queue, cl_queued;

struct layout cl_layout;
static bool cl_initial_layoutget;	/* TODO: errata */

static void _cl_recv_msg(struct msg *);

void
cl_init(void)
{
	queue_init(&cl_msg_queue);
	queue_init(&cl_queued);
	layout_init(&cl_layout, "client");
}

static struct msg *
cl_gen_rand_msg(void)
{
	struct msg *m;

	m = myzalloc(sizeof(*m));
	m->range.offset = mynrand(MAX_LO_MSG_OFFSET) & ~BLOCK_MASK;
	m->range.length = BLOCK_SIZE + mynrand(MAX_LO_MSG_LENGTH - BLOCK_SIZE);
	m->range.iomode = mynrand(2) ? IOMODE_RW : IOMODE_READ;
	m->stateid = cl_layout.stateid;
	m->status = -1;

	return m;
}

void
_dump_msg(int level, const char *func, struct msg *m)
{
	dprintk(level, "%s: m=%p type=%d "
		   "offset=%llu length=%llu iomode=%d stateid=%d:%llu "
		   "status=%d\n",
		func, m, m->type,
		llu(m->range.offset), llu(m->range.length), m->range.iomode,
		m->stateid.type, llu(m->stateid.seq),
		m->status);
}

void
cl_dump_queue(int level)
{
	struct msg *m;

	dprintk(level, "%s: count=%Zd\n", __func__, cl_queued.count);
	list_for_each_entry (m, &cl_queued.list, list)
		_dump_msg(level, __func__, m);
}

static void
cl_send_layoutget(void)
{
	struct msg *m;

	/* TODO: errata */
	if (cl_initial_layoutget) {
		dprintk(1, "%s: waiting on initial layoutget\n", __func__);
		return;
	}

	m = cl_gen_rand_msg();
	if (m == NULL) {
		dprintk(1, "%s: srv queue full\n", __func__);
		return;
	}

	if (cl_layout.stateid.type == OPEN_STATEID)
		cl_initial_layoutget = true;

	m->type = LAYOUTGET;
	dump_msg(m);
	enqueue(&srv_msg_queue, m);
}

static void
cl_send_layoutreturn(void)
{
	struct msg *m;

	if (cl_layout.stateid.type == OPEN_STATEID) {
		dprintk(1, "%s: no layout to return yet\n", __func__);
		return;
	}

	m = cl_gen_rand_msg();
	if (m == NULL) {
		dprintk(1, "%s: srv queue full\n", __func__);
		return;
	}

	m->type = LAYOUTRETURN;
	dump_msg(m);
	enqueue(&srv_msg_queue, m);
}

struct rand_event cl_send_events[] = {
	{ 50, cl_send_layoutget },
	{ 50, cl_send_layoutreturn },
};

static void
cl_send_msg(void)
{
	dprintk(1, "%s\n", __func__);
	rand_call(cl_send_events);
}

static bool
is_next(struct stateid *new, struct stateid *cur)
{
	if (cur->type == OPEN_STATEID)
		return new->seq == 1;
	else
		return new->seq == cur->seq + 1;
}

static bool
quiesced(void)
{
	dprintk(2, "%s: cl_msg_queue=%Zd cl_queued=%Zd\n",
		__func__, cl_msg_queue.count, cl_queued.count);
	return queue_empty(&cl_msg_queue) && queue_empty(&cl_queued);
}

static bool
cl_queue_msg(struct msg *m, const char *func)
{
	bool do_queue;

	if (m->stateid.type == OPEN_STATEID) /* terminating layoutreturn? */
		do_queue = !quiesced();
	else
		do_queue = !is_next(&m->stateid, &cl_layout.stateid);

	if (do_queue) {
		enqueue(&cl_queued, m);
		dprintk(1, "%s: queued m=%p m->stateid=%d:%llu stateid=%d:%llu\n",
			func, m, m->stateid.type, llu(m->stateid.seq),
			cl_layout.stateid.type, llu(cl_layout.stateid.seq));
	} else {
		dprintk(1, "%s: old_stateid=%d:%llu new_stateid=%d:%llu\n",
			__func__, cl_layout.stateid.type, llu(cl_layout.stateid.seq),
			m->stateid.type, llu(m->stateid.seq));
		cl_layout.stateid = m->stateid;
	}
	return do_queue;
}

static void
_reprocess_queue(void)
{
	struct msg *m;

	list_for_each_entry (m, &cl_queued.list, list) {
		if (m->type == CB_LAYOUT_RECALL)
			continue;
		if (is_next(&m->stateid, &cl_layout.stateid) ||
		    (m->type == LAYOUTRETURN &&
		     m->stateid.type == OPEN_STATEID &&
		     cl_queued.count == 1 &&
		     queue_empty(&cl_msg_queue))) {
			list_del(&m->list);
			cl_queued.count--;
			dprintk(1, "%s: m=%p count=%Zd\n",
				__func__, m, cl_queued.count);
			_cl_recv_msg(m);
			return;
		}
	}
}

static void
reprocess_queue(void)
{
	_reprocess_queue();
	if (quiesced())
		layout_verify(&srv_layout, &cl_layout);
}

static void
cl_recv_layoutget(struct msg *m)
{
	if (m->status)
		goto out_free;

	cl_initial_layoutget = false;

	if (cl_queue_msg(m, __func__))
		return;

	dprintk(1, "%s: inserting m=%p\n", __func__, m);
	layout_insert(&cl_layout, &m->range);
out_free:
	myfree(m);
}

static void
cl_recv_layoutreturn(struct msg *m)
{
	if (m->status)
		goto out_free;

	if (cl_queue_msg(m, __func__))
		return;

	dprintk(1, "%s: deleting m=%p\n", __func__, m);
	layout_delete(&cl_layout, &m->range);
out_free:
	myfree(m);
}

static void
cl_recv_cb_layout_recall(struct msg *m)
{
	if ((m->stateid.type == OPEN_STATEID &&
	     cl_layout.stateid.type != OPEN_STATEID) ||
	    (m->stateid.seq > cl_layout.stateid.seq + 1)) {
		dprintk(1, "%s: m=%p seq=%llu cl_layout.seq=%llu: waiting\n",
			__func__, m,
			llu(m->stateid.seq), llu(cl_layout.stateid.seq));
		m->status = NFS4ERR_DELAY;
		goto out;
	}

	BUG_ON(m->stateid.seq <= cl_layout.stateid.seq);
	if (mynrand(10) < 3) {
		dprintk(1, "%s: m=%p: simulating delay\n", __func__, m);
		m->status = NFS4ERR_DELAY;
		goto out;
	}

	/* forgetful model */
	layout_delete(&cl_layout, &m->range);
	cl_layout.stateid = m->stateid;
	m->status = NFS4ERR_NOMATCHING_LAYOUT;

out:
	dprintk(1, "%s: m=%p status=%d\n", __func__, m, m->status);
	enqueue(&srv_msg_queue, m);
}

static void
_cl_recv_msg(struct msg *m)
{
	switch (m->type) {
	case LAYOUTGET:
		cl_recv_layoutget(m);
		break;
	case LAYOUTRETURN:
		cl_recv_layoutreturn(m);
		break;
	case CB_LAYOUT_RECALL:
		cl_recv_cb_layout_recall(m);
		break;
	default:
		BUG_ON(true);
	}
	reprocess_queue();
}

void
cl_recv_msg(void)
{
	struct msg *m = dequeue(&cl_msg_queue);

	if (m == NULL) {
		dprintk(1, "%s: queue empty\n", __func__);
		return;
	}

	dump_msg(m);
	_cl_recv_msg(m);
}

struct rand_event cl_events[] = {
	{ 50, cl_send_msg },
	{ 50, cl_recv_msg },
};

void
cl_draw_event(void)
{
	rand_call(cl_events);
}
