/*
 * Copyright 2010 Benny Halevy <bhalevy@panasas.com>
 *
 * Licensed under the GPL version 2 license.
 * See "COPYING" for more information
 */

#include "rand.h"

void
rand_call(struct rand_event *ev)
{
	unsigned prob, r;

	for (r = mynrand(100), prob = ev->prob; r > prob; prob += (++ev)->prob)
		;

	ev->func();
}
