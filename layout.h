/*
 * Copyright 2010 Benny Halevy <bhalevy@panasas.com>
 *
 * Licensed under the GPL version 2 license.
 * See "COPYING" for more information
 */

#ifndef LAYOUT_H
#define LAYOUT_H

#include "types.h"
#include "list.h"

#define NFS4_MAX_UINT64 (~(u64)0)

#define BLOCK_BITS 2
#define BLOCK_SIZE (1 << BLOCK_BITS)
#define BLOCK_MASK (BLOCK_SIZE - 1)
#define MAX_LO_MSG_OFFSET 100
#define MAX_LO_MSG_LENGTH 100

enum iomode {
	IOMODE_READ = 1,
	IOMODE_RW   = 2,
	IOMODE_ANY  = 3
};

struct layout_range {
	u64 offset;
	u64 length;
	enum iomode iomode;
	bool roc;
};

enum stateid_type {
	OPEN_STATEID,
	LAYOUT_STATEID
};

struct stateid {
	enum stateid_type type;
	u64 seq; /* assumed it never wraps around in the sim. unless
		    type resets to OPEN_STATEID */
};

struct msg;

struct layout {
	struct stateid stateid;
	struct list_head segs;
	char *desc;
	struct msg *layout_recall_msg;
};

void layout_init(struct layout *, char *desc);
void layout_insert(struct layout *, struct layout_range *);
bool _lookup_layout(const char *func, struct layout *,
		    const struct layout_range *,
		    bool do_delete);

static inline bool has_layout(struct layout *lo, const struct layout_range *range)
{
	return _lookup_layout(__func__, lo, range, false);
}

static inline bool layout_delete(struct layout *lo, const struct layout_range *range)
{
	return _lookup_layout(__func__, lo, range, true);
}

bool layout_verify(struct layout *server, struct layout *client);

enum msg_type {
	INVALID_MSG = 0,
	LAYOUTGET,
	LAYOUTRETURN,
	CB_LAYOUT_RECALL,
};

enum msg_status {
	NFS4_OK = 0,
	NFS4ERR_DELAY = 10008,
	NFS4ERR_OLD_STATEID = 10024,
	NFS4ERR_BAD_STATEID = 10025,
	NFS4ERR_NOMATCHING_LAYOUT = 10060,
	NFS4ERR_RECALLCONFLICT = 10061,
};

struct msg {
	enum msg_type type;
	struct layout_range range;
	struct stateid stateid;

	enum msg_status status;
	struct list_head list;
};

void _dump_msg(int level, const char *func, struct msg *m);
#define dump_msg(m) _dump_msg(1, __func__, (m))

#endif /* LAYOUT_H */
