/*
 * Copyright 2010 Benny Halevy <bhalevy@panasas.com>
 *
 * Licensed under the GPL version 2 license.
 * See "COPYING" for more information
 */

#ifndef RAND_H
#define RAND_H

#include <stdlib.h>

static inline unsigned mysrand(unsigned seed)
{
	srandom(seed);
	return seed;
}

/*
 * return random number in the range 0..(n-1), inclusive
 */
static inline int
mynrand(int n)
{
	return ((unsigned long long)random() * n) / ((unsigned long long)RAND_MAX + 1UL);
}

struct rand_event {
	unsigned prob;	/* out of 100 */
	void (* func)(void);
};

void rand_call(struct rand_event *);

#endif /* RAND_H */
