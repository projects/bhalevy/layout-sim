/*
 * Copyright 2010 Benny Halevy <bhalevy@panasas.com>
 *
 * Licensed under the GPL version 2 license.
 * See "COPYING" for more information
 */

#include <time.h>
#include <unistd.h>

#include "debug.h"
#include "list.h"
#include "layout.h"
#include "rand.h"
#include "client.h"
#include "server.h"

char *progname;

int debug_level;

struct rand_event main_events[] = {
	{ 50, cl_draw_event },
	{ 50, srv_draw_event },
};

long long
kmg(char *s)
{
	long long n = strtoll(s, &s, 0);

	switch (*s) {
	case 'T': case 't': n <<= 40; break;
	case 'G': case 'g': n <<= 30; break;
	case 'M': case 'm': n <<= 20; break;
	case 'K': case 'k': n <<= 10; break;
	case 0: break;
	default:
		BUG_ON(true);
	}

	return n;
}

int
main(int argc, char **argv)
{
	int opt;
	unsigned seed = time(0);
	long long niter = 1000;
	long long i;
	char *p;

	progname = argv[0];
	while ((p = strchr(progname, '/')) != NULL)
		progname = p + 1;

	while ((opt = getopt(argc, argv, ":d:n:s:")) != -1) {
		switch (opt) {
		case 'd':
			debug_level = atoi(optarg);
			break;
		case 'n':
			niter = kmg(optarg);
			break;
		case 's':
			seed = atoi(optarg);
			break;
		default:
			fprintf(stderr, "Usage: %s [-s seed]\n", progname);
			exit(EXIT_FAILURE);
		}
	}
	
	dprintk(0, "%s -d %d -n %lld -s %u\n",
		progname, debug_level, niter, seed);

	mysrand(seed);

	cl_init();
	srv_init();

	for (i = 0; i < niter; i++) {
		dprintk(1, "iter %lld\n", i);
		rand_call(main_events);
	}

	if (!queue_empty(&srv_msg_queue)) {
		dprintk(1, "%s: draining server messages\n", __func__);
		while (!queue_empty(&srv_msg_queue))
			srv_recv_msg();
	}
	if (!queue_empty(&cl_msg_queue)) {
		dprintk(1, "%s: draining client messages\n", __func__);
		while (!queue_empty(&cl_msg_queue))
			cl_recv_msg();
	}

	if (debug_level)
		printf("%s -d %d -n %lld -s %u\n",
			progname, debug_level, niter, seed);
	if (!queue_empty(&cl_queued)) {
		printf("%s: ERROR: client queue not empty\n", progname);
		cl_dump_queue(0);
		exit(EXIT_FAILURE);
	}

	printf("%s: SUCCESS\n", progname);
	return 0;
}
