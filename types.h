/*
 * Copyright 2010 Benny Halevy <bhalevy@panasas.com>
 *
 * Licensed under the GPL version 2 license.
 * See "COPYING" for more information
 */

#ifndef TYPES_H
#define TYPES_H

#include <sys/types.h>

typedef int64_t s64;
typedef u_int64_t u64;
typedef int32_t s32;
typedef u_int32_t u32;
typedef int16_t s16;
typedef u_int16_t u16;
typedef int8_t s8;
typedef u_int8_t u8;

#define ll(x) ((long long)(x))
#define llu(x) ((unsigned long long)(x))

typedef _Bool bool;

enum {
        false   = 0,
        true    = 1
};

#define offsetof(type, member) __builtin_offsetof(type, member)

#define container_of(ptr, type, member) ({ \
	const typeof( ((type *)0)->member ) *__mptr = (ptr); \
	(type *)( (char *)__mptr - offsetof(type, member) ); \
})

#define prefetch(x) __builtin_prefetch(x)

#endif /* TYPES_H */
