/*
 * Copyright 2010 Benny Halevy <bhalevy@panasas.com>
 *
 * Licensed under the GPL version 2 license.
 * See "COPYING" for more information
 */

#ifndef DEBUG_H
#define DEBUG_H

#include <stdio.h>
#include <stdlib.h>

#define BUG_ON(_cond_) do { \
	if (_cond_) { \
		fflush(stdout); \
		fprintf(stderr, "BUG: %s:%d: %s\n", \
			__FILE__, __LINE__, #_cond_); \
		fflush(stderr); \
		*(int *)NULL = 0; \
	} \
} while (0)

extern int debug_level;

#define dprintk(level, args...) do { \
	if (level <= debug_level) \
		printf(args); \
} while (0)

#endif /* DEBUG_H */
