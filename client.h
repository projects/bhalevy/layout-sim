/*
 * Copyright 2010 Benny Halevy <bhalevy@panasas.com>
 *
 * Licensed under the GPL version 2 license.
 * See "COPYING" for more information
 */

#ifndef CLIENT_H
#define CLIENT_H

#include "layout.h"
#include "list.h"
#include "queue.h"

extern struct queue cl_msg_queue;
extern struct layout cl_layout;
extern struct queue cl_queued;

void cl_init(void);
void cl_recv_msg(void);
void cl_draw_event(void);
void cl_dump_queue(int level);

#endif /* CLIENT_H */
