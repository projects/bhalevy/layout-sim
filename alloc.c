/*
 * Copyright 2010 Benny Halevy <bhalevy@panasas.com>
 *
 * Licensed under the GPL version 2 license.
 * See "COPYING" for more information
 */

#include <stdlib.h>
#include <string.h>

#include "alloc.h"
#include "debug.h"
#include "list.h"
#include "types.h"

#define MYALLOC_POISON 0xdb

struct alloc_hdr {
	struct hlist_node hlist;
	size_t sz;
	u8 data[];
};

#define HASH_SZ ((1 << 12) - 1)

struct hlist_head *myalloc_tbl;

static inline size_t myalloc_hash_val(void *p)
{
	return (ulong)p % HASH_SZ;
}

void *
myalloc(size_t sz)
{
	int i;
	struct alloc_hdr *hdr = malloc(sizeof(*hdr) + sz);
	BUG_ON(hdr == NULL);

	if (!myalloc_tbl) {
		myalloc_tbl = malloc(HASH_SZ * sizeof(*myalloc_tbl));
		BUG_ON(myalloc_tbl == NULL);
		for (i = 0; i < HASH_SZ; i++)
			INIT_HLIST_HEAD(&myalloc_tbl[i]);
	}
	hdr->sz = sz;
	i = myalloc_hash_val(hdr->data);
	hlist_add_head(&hdr->hlist, &myalloc_tbl[i]);

	return hdr->data;
}

void
myfree(void *p)
{
	struct alloc_hdr *hdr;
	struct hlist_node *pos;

	BUG_ON(p == NULL);

	hlist_for_each (pos, &myalloc_tbl[myalloc_hash_val(p)]) {
		hdr = hlist_entry(pos, struct alloc_hdr, hlist);
		if (hdr->data == p) {
			hlist_del(pos);
			memset(hdr, MYALLOC_POISON, sizeof(hdr) + hdr->sz);
			free(hdr);
			return;
		}
	}

	BUG_ON(true);	/* double free? */
}
