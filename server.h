/*
 * Copyright 2010 Benny Halevy <bhalevy@panasas.com>
 *
 * Licensed under the GPL version 2 license.
 * See "COPYING" for more information
 */

#ifndef SERVER_H
#define SERVER_H

#include "layout.h"
#include "queue.h"

extern struct queue srv_msg_queue;
extern struct layout srv_layout;

void srv_init(void);
void srv_recv_msg(void);
void srv_draw_event(void);

#endif /* SERVER_H */
