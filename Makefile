
OBJECTS = \
	alloc.o \
	client.o \
	layout.o \
	layout-sim.o \
	queue.o \
	rand.o \
	server.o

HEADERS = \
	alloc.h \
	client.h \
	debug.h \
	layout.h \
	list.h \
	queue.h \
	rand.h \
	server.h \
	types.h

TARGET = layout-sim

CC = gcc
CFLAGS = -g -Wall

all: layout-sim

${OBJECTS} : $(HEADERS)

${TARGET}: ${OBJECTS}

clean:
	rm -f ${TARGET} ${OBJECTS} *~
