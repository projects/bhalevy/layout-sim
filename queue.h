/*
 * Copyright 2010 Benny Halevy <bhalevy@panasas.com>
 *
 * Licensed under the GPL version 2 license.
 * See "COPYING" for more information
 */

#ifndef QUEUE_H
#define QUEUE_H

#include <stdio.h>
#include <string.h>

#include "debug.h"
#include "layout.h"
#include "types.h"

struct queue {
	size_t count;
	struct list_head list;
};

static inline void queue_init(struct queue *q)
{
	q->count = 0;
	INIT_LIST_HEAD(&q->list);
}

static inline bool queue_empty(struct queue *q)
{
	BUG_ON((!q->count) ^ list_empty(&q->list));
	return q->count == 0;
}

int enqueue(struct queue *, struct msg *);
struct msg *dequeue(struct queue *);
struct msg *dequeue_rand(struct queue *);

#endif /* QUEUE_H */
