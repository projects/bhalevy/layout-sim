/*
 * Copyright 2010 Benny Halevy <bhalevy@panasas.com>
 *
 * Licensed under the GPL version 2 license.
 * See "COPYING" for more information
 */

#include "alloc.h"
#include "client.h"
#include "debug.h"
#include "layout.h"
#include "queue.h"
#include "rand.h"
#include "server.h"

struct queue srv_msg_queue;

struct layout srv_layout;

void
srv_init(void)
{
	queue_init(&srv_msg_queue);
	layout_init(&srv_layout, "server");
}

static void
srv_set_stateid(struct msg *m)
{
	if (m->type == LAYOUTRETURN && list_empty(&srv_layout.segs)) {
		m->stateid.type = OPEN_STATEID;
		m->stateid.seq = 0;
		goto out;
	}

	m->stateid.type = LAYOUT_STATEID;
	switch (srv_layout.stateid.type) {
	case OPEN_STATEID:
		m->stateid.seq = 1;
		break;
	case LAYOUT_STATEID:
		m->stateid.seq =  srv_layout.stateid.seq + 1;
		break;
	default:
		BUG_ON(true);
	}
out:
	dprintk(1, "%s: old_stateid=%d:%llu new_stateid=%d:%llu\n",
		__func__, srv_layout.stateid.type, llu(srv_layout.stateid.seq),
		m->stateid.type, llu(m->stateid.seq));
	srv_layout.stateid = m->stateid;
}

static int
verify_stateid(struct msg *m)
{
	switch (srv_layout.stateid.type) {
	case OPEN_STATEID:
		if (m->stateid.type != OPEN_STATEID)
			return m->status = NFS4ERR_BAD_STATEID;
		break;
	case LAYOUT_STATEID:
		if (m->stateid.type == OPEN_STATEID &&
		    srv_layout.stateid.seq > 100) {
			/* FIXME: use an actual limit on parallelism */
			return m->status = NFS4ERR_OLD_STATEID;
		} else if (m->stateid.type == LAYOUT_STATEID) {
			/* FIXME: is this specified for LAYOUTRETURN. */
			if (srv_layout.layout_recall_msg &&
		            m->stateid.seq < srv_layout.layout_recall_msg->stateid.seq)
				return m->status = NFS4ERR_OLD_STATEID;
			else if (m->stateid.seq > srv_layout.stateid.seq)
				return m->status = NFS4ERR_BAD_STATEID;
		}
		break;
	default:
		BUG_ON(true);
	}
	return 0;
}

static bool
is_conflicting(struct layout_range *lr, struct layout_range *r)
{
	if (lr->iomode != IOMODE_ANY && lr->iomode != r->iomode)
		return false;
	if (r->offset >= lr->offset)
		return (lr->length == NFS4_MAX_UINT64) ||
		       (lr->offset + lr->length > r->offset);
	else
		return (r->length == NFS4_MAX_UINT64) ||
		       (r->offset + r->length > lr->offset);
}

static void
srv_recv_layoutget(struct msg *m)
{
	struct msg *lrm = srv_layout.layout_recall_msg;

	/* See section 12.5.5.2.1.3 */
	if (lrm && is_conflicting(&lrm->range, &m->range)) {
		m->status = NFS4ERR_RECALLCONFLICT;
		goto out;
	}

	if (verify_stateid(m))
		goto out;

	/* randomize layoutget response */
	m->range.length = BLOCK_SIZE + mynrand(MAX_LO_MSG_LENGTH - BLOCK_SIZE);
	BUG_ON(m->range.iomode != IOMODE_READ && m->range.iomode != IOMODE_RW);
	if (m->range.iomode == IOMODE_READ && mynrand(2))
		m->range.iomode = IOMODE_RW;
	srv_set_stateid(m);
	m->status = NFS4_OK;
	layout_insert(&srv_layout, &m->range);
out:
	dump_msg(m);
}

static void
srv_recv_layoutreturn(struct msg *m)
{
	struct msg *lrm;

	if (verify_stateid(m))
		return;

	layout_delete(&srv_layout, &m->range);
	srv_set_stateid(m);
	m->status = NFS4_OK;
	lrm = srv_layout.layout_recall_msg;
	if (lrm && m->range.iomode == lrm->range.iomode &&
	    m->range.offset == lrm->range.offset &&
	    m->range.length && lrm->range.length) {
		srv_layout.layout_recall_msg = NULL;
		myfree(lrm);
	}
	dump_msg(m);
}

static void
issue_layout_recall(struct msg *m)
{
	/* bump server stateid.seq */
	srv_layout.stateid.seq++;
	m->stateid =  srv_layout.stateid;
	m->status = -1;
	srv_layout.layout_recall_msg = m;
	dump_msg(m);
	enqueue(&cl_msg_queue, m);
}

static void
srv_recv_cb_layout_recall(struct msg *m)
{
	BUG_ON(m != srv_layout.layout_recall_msg);
	dump_msg(m);
	switch (m->status) {
	case NFS4_OK:
		/* CB_LAYOUT_RECALL RPC is done but the meta-operations
		 * continues until after receiving the respective terminating
		 * LAYOUTRETURN
		 */
		break;
	case NFS4ERR_DELAY:
		dprintk(1, "%s: m=%p: layout_recall resent\n",
			__func__, srv_layout.layout_recall_msg);
		issue_layout_recall(srv_layout.layout_recall_msg);
		break;
	case NFS4ERR_NOMATCHING_LAYOUT:
		layout_delete(&cl_layout, &m->range);
		srv_layout.layout_recall_msg = NULL;
		myfree(m);
		break;
	default:
		BUG_ON(true);
	}
}

void
srv_recv_msg(void)
{
	struct msg *m = dequeue_rand(&srv_msg_queue);

	if (m == NULL) {
		dprintk(1, "%s: queue empty\n", __func__);
		return;
	}

	dump_msg(m);
	switch (m->type) {
	case LAYOUTGET:
		srv_recv_layoutget(m);
		break;
	case LAYOUTRETURN:
		srv_recv_layoutreturn(m);
		break;
	case CB_LAYOUT_RECALL:
		srv_recv_cb_layout_recall(m);
		return;
	default:
		BUG_ON(true);
	}
	enqueue(&cl_msg_queue, m);
}

static void
srv_recall_layout(void)
{
	struct msg *m;

	if (srv_layout.layout_recall_msg) {
		dprintk(1, "%s: m=%p layout_recall already in progress: status=%d\n",
			__func__,
			srv_layout.layout_recall_msg,
			srv_layout.layout_recall_msg->status);
		return;
	}

	dprintk(1, "%s\n", __func__);
	m = myzalloc(sizeof(*m));
	m->type = CB_LAYOUT_RECALL;
	m->range.offset = mynrand(MAX_LO_MSG_OFFSET) & ~BLOCK_MASK;
	m->range.length = BLOCK_SIZE + mynrand(MAX_LO_MSG_LENGTH - BLOCK_SIZE);
	m->range.iomode = IOMODE_READ + mynrand(3);
	issue_layout_recall(m);
}

struct rand_event srv_events[] = {
	{ 75, srv_recv_msg },
	{ 25, srv_recall_layout },
};

void
srv_draw_event(void)
{
	rand_call(srv_events);
}
