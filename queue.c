/*
 * Copyright 2010 Benny Halevy <bhalevy@panasas.com>
 *
 * Licensed under the GPL version 2 license.
 * See "COPYING" for more information
 */

#include "debug.h"
#include "queue.h"
#include "rand.h"

int
enqueue(struct queue *q, struct msg *m)
{
	dprintk(2, "%s: q=%p count=%Zd msg=%p\n", __func__, q, q->count, m);

	list_add_tail(&m->list, &q->list);
	return ++q->count;
}

static struct msg *
_dequeue(struct queue *q, int i)
{
	struct msg *m;

	if (queue_empty(q))
		return NULL;

	BUG_ON(i >= q->count);
	list_for_each_entry (m, &q->list, list)
		if (--i <= 0) {
			list_del(&m->list);
			q->count--;
			break;
		}
	dprintk(2, "%s: q=%p count=%Zd i=%d msg=%p\n",
		__func__, q, q->count, i, m);

	return m;
}

struct msg *
dequeue(struct queue *q)
{
	return _dequeue(q, 0);
}

struct msg *
dequeue_rand(struct queue *q)
{
	return _dequeue(q, mynrand(q->count));
}
