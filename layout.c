/*
 * Copyright 2010 Benny Halevy <bhalevy@panasas.com>
 *
 * Licensed under the GPL version 2 license.
 * See "COPYING" for more information
 *
 */

#include "alloc.h"
#include "debug.h"
#include "layout.h"

struct layout_item {
	struct layout_range range;
	struct list_head list;
};

void
layout_init(struct layout *lo, char *desc)
{
	lo->stateid.type = OPEN_STATEID;
	lo->stateid.seq = 0;
	INIT_LIST_HEAD(&lo->segs);
	lo->desc = desc;
}

void
layout_insert(struct layout *lo, struct layout_range *range)
{
	struct layout_item *l = myalloc(sizeof(*l));

	dprintk(1, "%s: %s: l=%p offset=%llu length=%llu iomode=%d roc=%d\n",
		__func__, lo->desc, l,
		llu(range->offset), llu(range->length),
		range->iomode, range->roc);
	l->range = *range;
	list_add(&l->list, &lo->segs);
}

static inline u64
end_offset(u64 start, u64 len)
{
	u64 end;

	end = start + len;
	return end >= start ? end : NFS4_MAX_UINT64;
}

/* last octet in a range */
static inline u64
last_byte_offset(u64 start, u64 len)
{
	u64 end;

	BUG_ON(!len);
	end = start + len;
	return end > start ? end - 1 : NFS4_MAX_UINT64;
}

/*
 * are two octet ranges overlapping?
 * start1            last1
 *   |-----------------|
 *                start2            last2
 *                  |----------------|
 */
static inline int
lo_seg_overlapping(const struct layout_range *l1, const struct layout_range *l2)
{
	u64 start1 = l1->offset;
	u64 last1 = last_byte_offset(start1, l1->length);
	u64 start2 = l2->offset;
	u64 last2 = last_byte_offset(start2, l2->length);
	int ret;

	/* if last1 == start2 there's a single byte overlap */
	ret = (last2 >= start1) && (last1 >= start2);
	dprintk(3, "%s: l1 %llu:%llu l2 %llu:%llu ret=%d\n", __func__,
		llu(l1->offset), llu(l1->length),
		llu(l2->offset), llu(l2->length), ret);
	return ret;
}

bool
_lookup_layout(const char *func, struct layout *lo,
	       const struct layout_range *range,
	       bool do_delete)
{
	bool found = false;
	struct layout_item *pos, *next;

	dprintk(1, "%s: %s: offset=%llu length=%llu iomode=%d roc=%d\n",
		func, lo->desc,
		llu(range->offset), llu(range->length),
		range->iomode, range->roc);

	list_for_each_entry_safe (pos, next, &lo->segs, list) {
		if ((range->iomode == IOMODE_ANY ||
		     range->iomode == pos->range.iomode) &&
		    (!range->roc || pos->range.roc) &&
		    lo_seg_overlapping(&pos->range, range)) {
			dprintk(1, "%s: found l=%p\n", __func__, pos);
			found = true;
			if (do_delete) {
				list_del(&pos->list);
				myfree(pos);
			} else
				break;
		}
	}

	return found;
}

bool
layout_verify(struct layout *server, struct layout *client)
{
	bool ok = true, found;
	struct layout_item *cl, *sl;

	if (((client->stateid.type != server->stateid.type) ||
	     (client->stateid.seq != server->stateid.seq)) &&
	    (!server->layout_recall_msg ||
	     client->stateid.seq >= server->stateid.seq)) {
		dprintk(0, "%s: cl stateid=%d:%llu srv stateid=%d:%llu\n",
			__func__,
			client->stateid.type, llu(client->stateid.seq),
			server->stateid.type, llu(server->stateid.seq));
		ok = false;
	}

	list_for_each_entry (cl, &client->segs, list) {
		found = false;
		list_for_each_entry (sl, &server->segs, list) {
			if ((cl->range.offset == sl->range.offset) &&
			    (cl->range.length == sl->range.length) &&
			    (cl->range.iomode == sl->range.iomode) &&
			    (cl->range.roc == sl->range.roc)) {
				found = true;
				break;
			}
		}
		if (!found) {
			ok = false;
			dprintk(0, "%s: cl offset=%llu length=%llu iomode=%d roc=%d: "
				"not found on server\n", __func__,
				llu(cl->range.offset), llu(cl->range.length),
				cl->range.iomode, cl->range.roc);
		}
	}
	dprintk(1, "%s: %s\n", __func__, ok ? "OK" : "ERROR");
	BUG_ON(!ok);
	return ok;
}
