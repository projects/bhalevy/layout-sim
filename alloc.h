/*
 * Copyright 2010 Benny Halevy <bhalevy@panasas.com>
 *
 * Licensed under the GPL version 2 license.
 * See "COPYING" for more information
 */

#ifndef ALLOC_H
#define ALLOC_H

#include <string.h>

void *myalloc(size_t sz);
void myfree(void *p);

static inline void *
myzalloc(size_t sz)
{
	void *p = myalloc(sz);
	memset(p, 0, sz);
	return p;
}

static inline void *
mycalloc(size_t nelem, size_t elem_sz)
{
	return myzalloc(nelem * elem_sz);
}

#endif /* ALLOC_H */
